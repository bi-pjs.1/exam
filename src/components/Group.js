import React, {PureComponent} from 'react';
import Task from './Task'
import AddTask from './AddTask'

export default class Group extends PureComponent {
    render() {
        // this.props.index contains group id (index in groups array)
        const tasks = this.props.tasks.map((task, index) => (
            <Task key={index} index={index} group={this.props.index} {...task} />));

        return (
            <div>
                <h2>{this.props.title}</h2>
                <ul>
                    <AddTask group={this.props.index} />
                    {tasks}
                </ul>
            </div>
        );
    }
}