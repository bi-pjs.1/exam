import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import Group from './Group'

class App extends PureComponent {
    render() {
        const groups = this.props.groups.map((group, index) => (<Group key={index} index={index} {...group} />));
        return (
            <div>
                <h1>Tasks</h1>
                {groups}
            </div>
        );
    }
}

// groups from redux store is mapped to this.props.groups
export default connect(
    (state) => ({
        groups: state.groups
    })
)(App);
