import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {toggleTask, deleteTask} from '../actions/task';

class Task extends PureComponent {

    onTaskChange = () => {
        // this.props.group contains group id (index in groups array)
        // this.props.index contains task id (index in task array)
        const { toggleTask, group, index } = this.props;
        // dispatch action
        toggleTask(group, index);
    };

    onDeleteClick = () => {
        const { deleteTask, group, index } = this.props;
        // dispatch action
        deleteTask(group, index);
    };

    render() {
        const { title, done } = this.props;
        return (
            <li>
                <label>
                    <input type="checkbox" checked={done} onChange={this.onTaskChange} />{title}
                </label>
                <input type="button" value="Delete" onClick={this.onDeleteClick} />
            </li>
        );
    }
}

// toggleTask and deleteTask action creators are mapped to this.props.toggleTask and this.props.deleteTask
export default connect(null, {toggleTask, deleteTask})(Task);