// action creators
export const ADD_TASK = 'ADD_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const TOGGLE_TASK = 'TOGGLE_TASK';

export function addTask(title, groupId) {
    return {
        type: ADD_TASK,
        payload: {
            title,
            groupId
        }
    }
}

export function deleteTask(groupId, taskId) {
    return {
        type: DELETE_TASK,
        payload: {
            groupId,
            taskId
        }
    }
}

export function toggleTask(groupId, taskId) {
    return {
        type: TOGGLE_TASK,
        payload: {
            groupId,
            taskId
        }
    }
}