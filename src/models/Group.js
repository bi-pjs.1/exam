export default class Group {
    constructor(title, tasks) {
        this.title = title;
        this.tasks = tasks || [];
    }

    addTask(task) {
        this.tasks.push(task);
    }
}