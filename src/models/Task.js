export default class Task {
    constructor(title, done) {
        this.title = title;
        this.done = done || false;
    }
}