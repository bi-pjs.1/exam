Závěrečná úloha
===============

Základní část (20 bodů)
-----------------------

- přeškrtnutí splněného úkolu
- přidání nové skupiny
- odstranění skupiny
- přidání a vypisování atributu úkolu `dueTo` (datum kdy má být úkol dokončen)
- u úkolů vypisovat počet dní do dokončení úkolu

Bonusová část (10 bodů)
-----------------------

- editace úkolu (názvu)
- editace skupiny (názvu)
- řazení úkolů podle data dokončení
- řazení skupin abecedně

Použití
=======

- `npm install`
- `npm start`

Nástroje
========

- [React Developer Tools](https://github.com/facebook/react-devtools)
- [Redux DevTools](http://extension.remotedev.io/)

